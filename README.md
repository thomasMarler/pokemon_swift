# Overview
Create a swift app with the Pokemon API 

# Home Page
PokemoHome : ![](https://bytebucket.org/thomasMarler/pokemon_swift/raw/b0945e5a2b1f531aaef522b4578895ca4d56d2cb/Pictures/main.png)

# Detail Page
PokemonDetail : ![](https://bytebucket.org/thomasMarler/pokemon_swift/raw/b0945e5a2b1f531aaef522b4578895ca4d56d2cb/Pictures/pokeDetail.png)