//
//  ViewController.swift
//  pokedexSwift3
//
//  Created by Tom Marler on 3/29/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var pokemon = [Pokemon]()
    var filterPokemon = [Pokemon]()
    var inSearchMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collection.dataSource = self
        collection.delegate = self
        searchBar.delegate = self
        
        searchBar.returnKeyType = UIReturnKeyType.done

        parsePokemonCSV()
        //let charmander = Pokemon(name: "Charmander", pokemonId: 4)
        
    }
    
    func parsePokemonCSV() {
        
        // Created a path to the CSV file
        let path = Bundle.main.path(forResource: "pokemon", ofType: "csv")!
        
        do {
            
            // Using the parse to pull out each row
            let csv = try CSV(contentsOfURL: path)
            let rows = csv.rows
            print(rows)
            
            // Get the name and ID
            for row in rows {
                let pokeId = Int(row["id"]!)!
                let name = row["identifier"]!
                
                // Create the Pokemon Object
                let poke = Pokemon(name: name, pokemonId: pokeId)
                
                // Append the Pokemon Object to the array
                pokemon.append(poke)
            }
            
            // If something goes wrong print Errror
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCell", for: indexPath) as? PokeCell {
            
            let poke: Pokemon!
            
            if inSearchMode {
                
                poke = filterPokemon[indexPath.row]
                cell.configureCell(poke)
                
            } else {
                
                poke = pokemon[indexPath.row]
                cell.configureCell(poke)
            }
            
            return cell
        }
        else {
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var poke: Pokemon!
        
        if inSearchMode {
            
            poke = filterPokemon[indexPath.row]
            
        } else {
            
            poke = pokemon[indexPath.row]
        }
        
        performSegue(withIdentifier: "PokemonDetailVC", sender: poke)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if inSearchMode {
            return filterPokemon.count
        }
        
        return pokemon.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 105, height: 105)
    }
    
    
    // Search bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // If the search bar is nill or string is nill
        if searchBar.text == nil || searchBar.text == "" {
            
            // We are not in search mode
            inSearchMode = false
            
            // Reload the collection view
            collection.reloadData()
            view.endEditing(true)
            
        } else {

            // We are in search mode
            inSearchMode = true
            
            let lower = searchBar.text!.lowercased()
            
            // Filter the pokemon
            filterPokemon = pokemon.filter({$0.name.range(of: lower) != nil})
            
            // Reload the collection view
            collection.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PokemonDetailVC" {
            if let detailsVC = segue.destination as? PokemonDetailVC {
                if let poke = sender as? Pokemon {
                    detailsVC.pokemon = poke
                }
            }
        }
    }
}

