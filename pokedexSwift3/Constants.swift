//
//  Constants.swift
//  pokedexSwift3
//
//  Created by Tom Marler on 4/2/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import Foundation


let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()
