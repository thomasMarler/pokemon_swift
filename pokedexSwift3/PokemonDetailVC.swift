//
//  PokemonDetailVC.swift
//  pokedexSwift3
//
//  Created by Tom Marler on 4/1/17.
//  Copyright © 2017 Tom Marler. All rights reserved.
//

import UIKit

class PokemonDetailVC: UIViewController {
    
    

    
    @IBOutlet weak var evoLabel: UILabel!
    @IBOutlet weak var nextEvoImage: UIImageView!
    @IBOutlet weak var currentEvoImage: UIImageView!
    @IBOutlet weak var attackLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var pokeIdLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var defenseLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descpLabel: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var pokemon: Pokemon!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(pokemon.attack)")
        
        nameLabel.text = pokemon.name
        
        let img = UIImage(named: "\(pokemon.pokemonId)")
        
        mainImage.image = img
        currentEvoImage.image = img
        pokeIdLabel.text = "\(pokemon.pokemonId)"
        pokemon.downloadPokemonDetail {
            
            self.updateUI()
            
        }
    }
    
    func updateUI() {
        
        attackLabel.text = pokemon.attack
        defenseLabel.text = pokemon.defense
        heightLabel.text = pokemon.height
        weightLabel.text = pokemon.weight
        typeLabel.text = pokemon.type
        descpLabel.text = pokemon.description
        
        if pokemon.nextEvoId == "" {
            evoLabel.text = "No Evolutions"
            nextEvoImage.isHidden = true
        } else {
            nextEvoImage.isHidden = false
            nextEvoImage.image = UIImage(named: self.pokemon.nextEvoId)
            let str = "Next Evolution \(pokemon.nextEvoName) - LVL \(pokemon.nextEvoLevel)"
            evoLabel.text = str
        }
        
        
    }
    
    @IBAction func btnPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }

    
    
}
